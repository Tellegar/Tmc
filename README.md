# Tmc - 1.12.2 Minecraft modpack
Tech/magic oriented modpack.

# Install instructions:
- Download and install any client;
- Download and install [Forge](https://files.minecraftforge.net/maven/net/minecraftforge/forge/index_1.12.2.html);
- Follow update instructions.

# Update instructions:
- Download zip of this repository;
- Extract it and paste it over your .minecraft directory for your client.
